extends RayCast2D

export var speed = 100000.0
export var time_to_overheat = 1.0

var is_fired = false setget fire_beam
var is_ai_weapon = false
var max_length = 0

onready var beam = $Beam
onready var charge_particles = $BarrelParticles
onready var beam_particles = $BeamParticles
onready var collision_particles = $CollisionParticles
onready var tween = $Tween
onready var beam_width = beam.width
onready var overheat_timer = $OverheatTimer
onready var sfx = $SFX

func _ready():
	
	set_physics_process(false)
	max_length = get_viewport_rect().size.x / get_parent().scale.x # set laser beam max_length to screen width
	beam.points[1] = Vector2.ZERO # set initial lenght to zero
	overheat_timer.wait_time = time_to_overheat


func _physics_process(delta):

	cast_to = (cast_to + Vector2.RIGHT * speed * delta).clamped(max_length) # tell raycast where and how far to fire
	update_beam()


func update_beam():
	
	var target_point = cast_to # set target point
	
	force_raycast_update() # force update collision info
	
	if is_colliding():
		
		var victim = get_collider()
		victim.hit(get_parent())
		
		target_point = to_local(get_collision_point()) # set target point to collision point
		collision_particles.emitting = true # emit collision particles
		collision_particles.global_rotation = get_collision_normal().angle() # set rotation for collision particles
		collision_particles.position = target_point # set emitting point for collision particles
	else:
		collision_particles.emitting = false
	
	beam.points[1] = target_point
	beam_particles.position = target_point * 0.5
	beam_particles.emission_rect_extents.x = target_point.length() * 0.5


func fire_beam(fired):
	
	is_fired = fired
	
	set_physics_process(is_fired)
	charge_particles.emitting = is_fired
	beam_particles.emitting = is_fired
	
	if is_fired:
		if Globals.sound:
			sfx.play()
		if is_ai_weapon:
			overheat_timer.start()
		cast_to = Vector2.ZERO
		beam.points[1] = cast_to
		appear()
	else:
		sfx.stop()
		collision_particles.emitting = false
		disappear()


func appear():
	
	if tween.is_active():
		tween.stop_all()
	tween.interpolate_property(beam, "width", 0, beam_width, 0.2)
	tween.start()


func disappear():
	
	if tween.is_active():
		tween.stop_all()
	tween.interpolate_property(beam, "width", beam_width, 0, 0.2)
	tween.start()


func _on_OverheatTimer_timeout():
	
	get_parent().cooldown()
	fire_beam(false)
