extends Node2D

signal report_ufo_down

var in_position = false
var scattered = false
var squad = []
var direction = -1
var anchors_set = false
var anchor_mid = 0
var anchor_min = 0
var anchor_max = 0

onready var arrival_timer = $ArrivalTimer


func _ready():
	
	arrival_timer.start()
	
	squad = $UFOs.get_children()
	
	for ufo in squad:
		ufo.connect("shot_down", self, "on_ufo_down")


func _physics_process(delta):
	
	if in_position:
		if !anchors_set:
			anchor_mid = position.y
			anchor_min = anchor_mid - 50
			anchor_max = anchor_mid + 50
			anchors_set = true
		if (direction<0 and position.y <= anchor_min) or (direction>0 and position.y >= anchor_max):
			direction *= -1
		position.y += direction * Globals.move_speed/2 * delta

func on_ufo_down():
	
	emit_signal("report_ufo_down")


func _on_ArrivalTimer_timeout():
	
	in_position = true
