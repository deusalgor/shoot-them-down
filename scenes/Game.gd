extends Node2D

onready var viewport_rect = get_viewport_rect()
onready var charge_bar = $CanvasLayer/UI/ChargeBar
onready var health_label = $CanvasLayer/UI/LivesLabel
onready var health_icon = $CanvasLayer/UI/LivesIcon
onready var points_label = $CanvasLayer/UI/MatchPoints
onready var end_game_msg = $CanvasLayer/UI/EndGameMsg
onready var restart_btn = $CanvasLayer/UI/RestartBtn
onready var pause_btn = $CanvasLayer/UI/PauseBtn
onready var exit_btn = $CanvasLayer/UI/ExitBtn
onready var play_btn = $CanvasLayer/UI/PlayBtn
onready var mute_btn = $CanvasLayer/UI/MuteBtn
onready var music = $Music

var max_allowed_height = 0
var min_allowed_height = 0

var player
var wave = 0
var waves = {
	wave1 = {
		squad1 = [0,0]
	},
	wave2 = {
		squad1 = [0,100],
		squad2 = [0,-100] 
	},
	wave3 = {
		squad1 = [-50,100],
		squad2 = [-50,-100],
		squad3 = [50,150],
		squad4 = [50,-150]  
	}
}

var ufos_alive = 0
var match_points = 0

func _ready():
	
	randomize()
	
	end_game_msg.text = Globals.endgame_messages[randi() % Globals.endgame_messages.size()]
	
	set_physics_process(false)
	
	max_allowed_height = viewport_rect.size.y/2 - viewport_rect.size.y * 0.45
	min_allowed_height = viewport_rect.size.y/2 + viewport_rect.size.y * 0.45
	


func _physics_process(delta):
	
	if ufos_alive <= 0:
		spawn_ufo()
	
	if player.laser.is_fired:
		Globals.charge -= 10 * delta


func spawn_player():
	
	player = preload("res://scenes/Player.tscn").instance()
	get_tree().call_group("Hud", "show")
	add_child(player)
	
	player.connect("player_down", self, "on_player_down")
	
	player.global_position = Vector2(viewport_rect.size.x/10, viewport_rect.size.y/2)
	player.max_height = max_allowed_height
	player.min_height = min_allowed_height



func spawn_ufo():
	
	wave += 1
	
	var spawn_x = viewport_rect.size.x - viewport_rect.size.x/8
	var spawn_y = viewport_rect.size.y/2
	
	var wave_index = "wave"
	if wave <=3 : wave_index += str(wave)
	else: wave_index = "wave3"
	
	for el in waves[wave_index]:
		var squad_data = waves[wave_index][el]
		var squad = preload("res://scenes/BasicUFOSquad.tscn").instance()
		add_child(squad)
		ufos_alive += 2
		squad.global_position = Vector2(spawn_x - squad_data[0], spawn_y + squad_data[1])
		squad.connect("report_ufo_down", self, "on_ufo_down_report")


func on_ufo_down_report():
	
	ufos_alive -= 1
	match_points += 1 * wave
	update_points()


func update_points():
	
	points_label.text = "%s"%match_points
	
	if Globals.charge < Globals.max_charge: 
		Globals.charge += match_points
	
	if Globals.move_speed <= 350:
		Globals.move_speed = Globals.default_move_speed + match_points / 10
		Globals.charge_leak = Globals.default_charge_leak + match_points / 10
	
	


func _on_RestartBtn_pressed():
	
# warning-ignore:return_value_discarded
	get_tree().paused = false
	get_tree().reload_current_scene()
	

func _on_ExitBtn_pressed():
	
	get_tree().quit()


func on_player_down():

	toggle_menu()
	end_game_msg.show()
	pause_btn.hide()


func toggle_menu():
	
	if restart_btn.visible:
		get_tree().call_group("Menu", "hide")
		get_tree().paused = false
	else:
		get_tree().call_group("Menu", "show")
		get_tree().paused = true


func _on_PauseBtn_pressed():

	toggle_menu()


func _on_PlayBtn_pressed():
	
	play_btn.hide()
	mute_btn.hide()
	exit_btn.hide()
	pause_btn.show()
	
	Globals.charge = 100
	
	get_tree().call_group("Info", "hide")
	
	set_physics_process(true)
	spawn_player()
	update_points()


func _on_MuteBtn_pressed():
	
	Globals.sound = !mute_btn.pressed
	music.playing = Globals.sound


func _on_CredsTimer_timeout():
	
	$CanvasLayer/UI/PlayOnLoopCreds.hide()
