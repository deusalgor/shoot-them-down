extends Area2D

signal shot_down

var alive = true
var shielded = true
var direction = 0
var anchor_mid = 0
var anchor_min = 0
var anchor_max = 0


onready var animation = $AnimationPlayer
onready var laser = $LaserBeam
onready var laser_charge_particles = $LaserChargeParticles
onready var hull = $Hull
onready var laser_cooldown_timer = $LaserCooldown
onready var laser_charge_timer = $LaserCharge
onready var shield_collider = $ShieldCollider
onready var shield_sprite = $ShieldSprite
onready var warp_sfx = $WarpSFX

func _ready():
	
	animation.play("Arrive")
	if Globals.sound:
		warp_sfx.play()
	
	anchor_mid = position.x
	anchor_min = anchor_mid - 30
	anchor_max = anchor_mid + 30
	randomize()
	var directions = [-1,1]
	direction = directions[randi() % directions.size()]
	
	laser.is_ai_weapon = true


func _physics_process(delta):
	
	if !alive:
		position.y += 300 * delta
	elif !shielded:
		if (direction<0 and position.x <= anchor_min) or (direction>0 and position.x >= anchor_max):
			direction *= -1
		position.x += direction * Globals.move_speed/4 * delta

func _on_VisibilityNotifier2D_screen_exited():
	
	emit_signal("shot_down")
	queue_free()


func _on_BasicUFO_area_entered(area):
	
	hit(area)


func hit(attacker):
	
	if alive and !shielded: 
		alive = false
		if attacker.name == "Player":
			attacker.spawn_cell(global_position)
		if laser.is_fired:
			laser.fire_beam(false)
		


func charge_lasers():
	
	shielded = false
	laser_charge_particles.emitting = true
	shield_collider.disabled = true
	shield_sprite.hide()
	laser_charge_timer.start()


func cooldown():
	
	laser_cooldown_timer.start()

func _on_LaserCooldown_timeout():
	
	charge_lasers()


func _on_LaserCharge_timeout():
	
	if alive:
		laser.fire_beam(true)
